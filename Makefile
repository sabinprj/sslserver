CC := gcc
CFLAGS :=-std=c11 -Wall -pedantic -Wextra
LDFLAGS:=-lssl -lcrypto
HEADERS := $(wildcard *.h)
CFILES := $(wildcard *.c)
OFILES := $(patsubst %.c,%.o,$(CFILES))
APP = sslserver
ifeq ($(shell uname), Darwin)
	INCLUDES=-I/usr/local/opt/openssl/include
endif

all: install

install: $(APP)

$(APP): $(OFILES)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $^ $(LDFLAGS)

debug: CFLAGS+=-g
debug: install

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@
clean:
	$(RM) -r *.dSYM $(APP) $(OFILES)

.PHONY: all install $(APP) debug clean
