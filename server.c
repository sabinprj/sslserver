#include "server.h"

#define PORT "1443"

static const char *cert_ec = "certs/chained.ec.crt";
static const char *pkey_ec = "certs/sandbox.net.ec.key";
static const char *cert_rsa = "certs/chained.rsa.crt";
static const char *pkey_rsa = "certs/sandbox.net.rsa.key";
static const char *ciphers = "ECDH+AESGCM:ECDH+CHACHA20:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS:!3DES:@STRENGTH";

int main(void)
{
    int sockfd = setup_socket();
    if (sockfd == -1) {
        return 1;
    }

    SSL_CTX *ctx = setup_ssl_server();

    printf("* Listening on 0.0.0.0:%s\n", PORT);
    while (1) {
        handle_ssl_clients(sockfd, ctx);
    }

    SSL_CTX_free(ctx);
    ctx = NULL;
    close(sockfd);
    EVP_cleanup();
}

int setup_socket(void)
{
    struct addrinfo hints;
    struct addrinfo *res;

    memset(&hints, 0, sizeof (hints));

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int status = getaddrinfo(NULL, PORT, &hints, &res);
    if (status == -1) {
        fprintf(stderr, "Error: %s\n", gai_strerror(status));
        return -1;
    }

    int sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if(sockfd == -1) {
        perror("socket creation failed!");
        freeaddrinfo(res);
        return -1;
    }

    int yes = 1;
    if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) {
        perror("setsockopt");
        return -1;
    }

    if(setsockopt(sockfd, SOL_SOCKET, SO_KEEPALIVE, &yes, sizeof yes) == -1) {
        perror("setsockopt");
        return -1;
    }

    if (bind(sockfd, res->ai_addr, res->ai_addrlen) == -1) {
        perror("error while binding to a socket");
        freeaddrinfo(res);
        return -1;
    }

    if (listen(sockfd, 10) == -1) {
        perror("listen");
        freeaddrinfo(res);
        return -1;
    }

    freeaddrinfo(res);

    return sockfd;
}

void handle_clients(int sockfd)
{
    struct sockaddr_storage remaddr;
    socklen_t addr_size;

    addr_size = sizeof remaddr;
    int newfd = accept(sockfd, (struct sockaddr *) &remaddr, &addr_size);

    if (newfd == -1) {
        perror("accept");
        return;
    }
    printf("Got a connection!\n");

    char *msg = "Hello, world\r\n";
    size_t msglen = strlen(msg);
    ssize_t bytes_sent = send(newfd, msg, msglen, 0);
    printf("|>>: (%ld) %s", bytes_sent, msg);
    close(newfd);
    close(sockfd);
}

SSL_CTX *setup_ssl_server()
{
    SSL_load_error_strings();
    SSL_library_init();
    OpenSSL_add_all_algorithms();

    SSL_CTX *ctx = SSL_CTX_new(SSLv23_server_method());

    if (ctx == NULL) {
        fprintf(stderr, "failed to create new SSL context:\n");
        ERR_print_errors_fp(stderr);
        return NULL;
    }

    SSL_CTX_set_options(ctx, SSL_OP_SINGLE_DH_USE);

    /* Load certificates */
    if (load_certs(ctx) != 1) {
        return NULL;
    }
    return ctx;
}

unsigned load_certs(SSL_CTX *ctx)
{
    if (SSL_CTX_use_certificate_chain_file(ctx, cert_ec) != 1) {
        fprintf(stderr, "Failed to load EC server certificate: '%s'\n",
                cert_ec);
        ERR_print_errors_fp(stderr);
        return 0;
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, pkey_ec, SSL_FILETYPE_PEM) != 1) {
        fprintf(stderr, "Failed to load EC private key: '%s'\n", pkey_ec);
        ERR_print_errors_fp(stderr);
        return 0;
    }

    if (SSL_CTX_use_certificate_chain_file(ctx, cert_rsa) != 1) {
        fprintf(stderr, "Failed to load server certificate: '%s'\n",
                cert_rsa);
        ERR_print_errors_fp(stderr);
        return 0;
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, pkey_rsa, SSL_FILETYPE_PEM) != 1) {
        fprintf(stderr, "Failed to load private key: '%s'\n", pkey_rsa);
        ERR_print_errors_fp(stderr);
        return 0;
    }

    if (SSL_CTX_check_private_key(ctx) != 1) {
        fprintf(stderr, "Private key checking failed!");
        ERR_print_errors_fp(stderr);
        return 0;
    }

    if (SSL_CTX_set_ecdh_auto(ctx, 1) == 0) {
        fprintf(stderr, "SSL_CTX_set_ecdh_auto");
        ERR_print_errors_fp(stderr);
        return 0;
    }

    return 1;
}

void handle_ssl_clients(int sockfd, SSL_CTX *ctx)
{
    struct sockaddr_storage remaddr;
    SSL *ssl;

    socklen_t addr_size = sizeof remaddr;

    int clientfd = accept(sockfd, (struct sockaddr *) &remaddr, &addr_size);

    if (clientfd == -1) {
        perror("accept");
        return;
    }

    char client_ip[INET6_ADDRSTRLEN];

    printf("Client: '%s' with socket fd '%d'\n",
            inet_ntop(remaddr.ss_family,
                get_in_addr((struct sockaddr *) &remaddr), client_ip,
                            INET6_ADDRSTRLEN),
            clientfd);

    if ((ssl = SSL_new(ctx)) == NULL) {
        fprintf(stderr, "Failed to create a new SSL context!\n");
        ERR_print_errors_fp(stderr);
        close(clientfd);
        return;
    }


    if (SSL_set_cipher_list(ssl, ciphers) == 0) {
        fprintf(stderr, "SSL_set_cipher_list\n");
        ERR_print_errors_fp(stderr);
        SSL_free(ssl);
        ssl = NULL;
        close(clientfd);
        return;
    }

    if ((SSL_set_fd(ssl, clientfd)) == 0) {
        fprintf(stderr, "SSL_set_fd\n");
        ERR_print_errors_fp(stderr);
        SSL_free(ssl);
        ssl = NULL;
        close(clientfd);
        return;
    }

    serve(ssl);
    SSL_free(ssl);
    ssl = NULL;
    close(clientfd);
}

void ossl_err(SSL *ssl, int ret)
{
    int rc = SSL_get_error(ssl, ret);

    switch (rc) {
        case SSL_ERROR_NONE:
            fprintf(stderr, "SSL_ERROR_NONE\n");
            break;
        case SSL_ERROR_ZERO_RETURN:
            fprintf(stderr, "SSL_ERROR_ZERO_RETURN\n");
            break;
        case SSL_ERROR_WANT_READ:
        case SSL_ERROR_WANT_WRITE:
            fprintf(stderr, "SSL_ERROR_WANT_READ|SSL_ERROR_WANT_WRITE\n");
            break;
        case SSL_ERROR_WANT_CONNECT:
        case SSL_ERROR_WANT_ACCEPT:
            fprintf(stderr, "SSL_ERROR_WANT_CONNECT|SSL_ERROR_WANT_ACCEPT\n");
            break;
        case SSL_ERROR_WANT_X509_LOOKUP:
            fprintf(stderr, "SSL_ERROR_WANT_X509_LOOKUP\n");
            break;
        case SSL_ERROR_SYSCALL:
            fprintf(stderr, "SSL_ERROR_SYSCALL: ");
            if (ERR_peek_error() != 0) {
                ERR_print_errors_fp(stderr);
            } else {
                if (ret == -1) {
                    fprintf(stderr, "%s\n", strerror(errno));
                } else {
                    fprintf(stderr, "Client disconnected\n");
                }
            }
            break;
        case SSL_ERROR_SSL:
            fprintf(stderr, "SSL_ERROR_SSL: ");
            ERR_print_errors_fp(stderr);
            break;
        default:
            fprintf(stderr, "Unknown\n");
            break;
    }
}
void serve(SSL *ssl)
{
    char buf[1024];

    int ret;
    const char *response = "Hello, world\r\n";

    if ((ret = SSL_accept(ssl)) <= 0) {
        fprintf(stderr, "SSL_accept: ");
        ossl_err(ssl, ret);
        ERR_print_errors_fp(stderr);
        return;
    }

    show_client_certs(ssl);

    int sd = SSL_get_fd(ssl);
    if (sd == -1) {
        fprintf(stderr, "SSL_get_fd failed\n");
        return;
    }

    while (1) {
        int bytes = SSL_read(ssl, buf, sizeof(buf) - 1);

        if (bytes <= 0) {
            fprintf(stderr, "SSL_read: ");
            ossl_err(ssl, bytes);
            ERR_print_errors_fp(stderr);
            return;
        }

        if (bytes == 0) {
            break;
        }

        buf[bytes] = 0;
        printf("<<: %s", buf);

        int outbytes;
        if ((outbytes = SSL_write(ssl, response, strlen(response))) <= 0) {
            fprintf(stderr, "SSL_write: ");
            ossl_err(ssl, outbytes);
            ERR_print_errors_fp(stderr);
            close(sd);
            return;
        }
    }

    printf("Closing socket\n");
    close(sd);
}

void show_client_certs(SSL *ssl)
{
    X509 *cert;
    char *line;

    cert = SSL_get_peer_certificate(ssl);

    if (cert == NULL) {
        fprintf(stderr, "No client certificates!\n");
        return;
    }

    printf("Server certificates:\n");
    line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
    printf("Subject: %s\n", line);
    free(line);

    line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
    printf("Issuer: %s\n", line);
    free(line);

    X509_free(cert);
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}
