FROM alpine

RUN apk add --no-cache \
        build-base \
        ca-certificates \
        openssl-dev

RUN update-ca-certificates

WORKDIR /sslserv
COPY . .
RUN make install

EXPOSE 1443

ENTRYPOINT ["./sslserver"]
