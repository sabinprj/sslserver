#ifndef sslserver
#define sslserver

#define _POSIX_C_SOURCE 200809L
#include <string.h>
#include <errno.h>
#include <unistd.h>

#ifdef __linux__
#include <sys/types.h>
#endif
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

int setup_socket(void);
void handle_clients(int sockfd);
void handle_ssl_clients(int sockfd, SSL_CTX *ctx);
SSL_CTX *setup_ssl_server();
void dump_ossl_error();
unsigned load_certs();
void serve(SSL *ssl);
void show_client_certs(SSL *ssl);
void *get_in_addr(struct sockaddr *sa);
void ossl_err(SSL *ssl, int ret);
#endif
